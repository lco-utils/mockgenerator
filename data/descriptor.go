package data

type Descriptor struct {
	Package string
	Name    string
	Methods []MethodDescriptor
}

type MethodDescriptor struct {
	Name string
	Args string
	Retr string
}

type ArgDescriptor struct {
	Name string
	Type string
}
