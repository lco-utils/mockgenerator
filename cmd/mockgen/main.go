package main

import (
	"log"
	"os"

	"gitlab.com/lco-utils/mockgenerator/generators"
	"gitlab.com/lco-utils/mockgenerator/parsers"
)

func main() {
	if len(os.Args) != 2 || len(os.Args[1]) == 0 {
		log.Fatalf("Invalid arguments!")
	}
	fileName := os.Args[1]

	file, err := os.Open(fileName)
	if err != nil {
		log.Fatalf("Failed to open file '%s': %v", fileName, err)
	}
	defer file.Close()

	parser := parsers.NewDumbParser()

	descriptor, err := parser.Consume(file)
	if err != nil {
		log.Fatalf("Failed to parse file '%s': %v", fileName, err)
	}

	generator := generators.NewGenerator()

	generator.Handle(descriptor, os.Stdout)
}
