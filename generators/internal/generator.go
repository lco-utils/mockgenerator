package internal

import (
	"bufio"
	"fmt"
	"io"

	"strings"

	"gitlab.com/lco-utils/mockgenerator/data"
)

type GeneratorFn func(*data.Descriptor, io.Writer)

func (f GeneratorFn) Handle(descriptor *data.Descriptor, writer io.Writer) {
	f(descriptor, writer)
}

func Generate(d *data.Descriptor, wr io.Writer) {
	writer := bufio.NewWriter(wr)

	writer.WriteString("package mock\n")
	//TODO imports?
	writer.WriteString("\n")

	writer.WriteString(fmt.Sprintf("// %s mock\n", d.Name))
	writer.WriteString(fmt.Sprintf("type %s struct {\n", d.Name))

	for _, m := range d.Methods {
		retr := m.Retr
		if len(m.Retr) > 0 && strings.Contains(retr, `, `) {
			retr = fmt.Sprintf("(%s)", retr)
		}
		writer.WriteString(fmt.Sprintf("%sFn func(%s) %s\n", m.Name, m.Args, retr))
	}

	writer.WriteString("\n")

	for _, m := range d.Methods {
		writer.WriteString(fmt.Sprintf("%sInvoked bool\n", m.Name))
	}

	writer.WriteString("}\n\n")

	writer.WriteString(fmt.Sprintf("var _ %s.%s = (*%s)(nil)\n\n", d.Package, d.Name, d.Name))

	for _, m := range d.Methods {
		retr := m.Retr
		if len(m.Retr) > 0 && strings.Contains(retr, `, `) {
			retr = fmt.Sprintf("(%s)", retr)
		}
		//todo write only the argument names
		writer.WriteString(fmt.Sprintf("func (m *%s) %s(%s) %s {\n", d.Name, m.Name, m.Args, retr))
		writer.WriteString(fmt.Sprintf("m.%sInvoked = true\n", m.Name))
		if retr != "" {
			writer.WriteString(fmt.Sprintf("return m.%sFn(%s)\n", m.Name, m.Args))
		} else {
			writer.WriteString(fmt.Sprintf("m.%sFn(%s)\n", m.Name, m.Args))
		}
		writer.WriteString("}\n")
	}
	writer.Flush()
}
