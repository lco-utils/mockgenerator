package generators

import (
	"os"
	"testing"

	"gitlab.com/lco-utils/mockgenerator/data"
)

var descriptor = &data.Descriptor{
	Package: "stuff",
	Name:    "SomeInterface",
	Methods: []data.MethodDescriptor{
		{Name: "Method1", Args: "", Retr: ""},
		{Name: "ResetMount", Args: "ctx context.Context, lvPath string, mountPath string, onMountFn func(context.Context, string)", Retr: "context.Context, error"},
		{Name: "ResetMount", Args: "ctx context.Context, lvPath string, mountPath string, onMountFn func(context.Context, string)", Retr: "error"},
	},
}

func TestGenerator_Handle(t *testing.T) {
	generator := NewGenerator()

	generator.Handle(descriptor, os.Stdout)
}
