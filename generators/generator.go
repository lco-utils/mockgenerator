package generators

import (
	"io"

	"gitlab.com/lco-utils/mockgenerator/data"
	"gitlab.com/lco-utils/mockgenerator/generators/internal"
)

// Generator defines the handler for the code generation
type Generator interface {
	Handle(*data.Descriptor, io.Writer)
}

func NewGenerator() Generator {
	return internal.GeneratorFn(internal.Generate)
}
