package internal

import (
	"bufio"
	"io"

	"regexp"
	"strings"

	"github.com/go-errors/errors"
	"gitlab.com/lco-utils/mockgenerator/data"
)

var (
	packageRegexp       *regexp.Regexp
	interfaceNameRegexp *regexp.Regexp
	methodRegexp        *regexp.Regexp
	stopRegexp          *regexp.Regexp
)

func init() {
	packageRegexp = regexp.MustCompile(`^package ([a-zA-Z0-9_]+)$`)
	interfaceNameRegexp = regexp.MustCompile(`^type ([a-zA-Z0-9_]+) interface {$`)
	methodRegexp = regexp.MustCompile(`^([a-zA-Z0-9_]+)(\(.+)$`)
	stopRegexp = regexp.MustCompile(`^\}$`)
}

type ParserFn func(io.Reader) (*data.Descriptor, error)

func (f ParserFn) Consume(reader io.Reader) (*data.Descriptor, error) {
	return f(reader)
}

const (
	outsideInterfaceCode = iota
	withinInterfaceCode
)

func DumbParse(reader io.Reader) (*data.Descriptor, error) {
	position := outsideInterfaceCode

	descriptor := &data.Descriptor{Methods: make([]data.MethodDescriptor, 0, 1)}

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())

		switch position {
		case outsideInterfaceCode:
			if packageRegexp.MatchString(line) {
				descriptor.Package = packageRegexp.FindStringSubmatch(line)[1]
			}
			if !interfaceNameRegexp.MatchString(line) {
				continue
			}
			descriptor.Name = interfaceNameRegexp.FindStringSubmatch(line)[1]
			position = withinInterfaceCode
		case withinInterfaceCode:
			if stopRegexp.MatchString(line) {
				break
			}
			if methodRegexp.MatchString(line) {
				md := parseMethod(line)
				descriptor.Methods = append(descriptor.Methods, *md)
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return nil, errors.Errorf("Failed to scan reader: %v", err)
	}

	return descriptor, nil
}

func parseMethod(line string) *data.MethodDescriptor {
	groups := methodRegexp.FindStringSubmatch(line)
	argsAndRetrStr := strings.TrimSpace(groups[2])

	index := findClosingParenthesisIndex(argsAndRetrStr)

	args := strings.TrimSpace(argsAndRetrStr[0:index])
	retr := strings.TrimSpace(argsAndRetrStr[index:])

	// remove parenthesis
	args = args[1 : len(args)-1]
	if len(retr) > 0 && retr[0] == '(' {
		retr = retr[1 : len(retr)-1]
	}

	return &data.MethodDescriptor{
		Name: groups[1],
		Args: args,
		Retr: retr,
	}
}

func findClosingParenthesisIndex(str string) int {
	depth := 0
	for i, c := range str {
		if c == '(' {
			depth += 1
			continue
		}
		if c == ')' {
			depth -= 1
			continue
		}
		if depth == 0 {
			return i
		}
	}
	return len(str)
}
