package parsers

import (
	"io"

	"gitlab.com/lco-utils/mockgenerator/data"
	"gitlab.com/lco-utils/mockgenerator/parsers/internal"
)

type Parser interface {
	Consume(io.Reader) (*data.Descriptor, error)
}

func NewDumbParser() Parser {
	return internal.ParserFn(internal.DumbParse)
}
