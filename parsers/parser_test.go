package parsers

import (
	"fmt"
	"strings"
	"testing"
)

func TestParser_Consume(t *testing.T) {
	str := `package hello
		type SomeInterface interface {
		Method1()
		ResetMount(ctx context.Context, lvPath string, mountPath string, onMountFn func(context.Context, string)) (context.Context, error)
		ResetMount(ctx context.Context, lvPath string, mountPath string, onMountFn func(context.Context, string)) error
	}`

	descriptor, err := NewDumbParser().Consume(strings.NewReader(str))
	if err != nil {
		t.Errorf("Failed at read: %s", err)
	}

	fmt.Printf("Descriptor: %+v\n", descriptor)
}
