#!/bin/sh



function build {
    echo [CLEAN]
    go clean
    rm $GOPATH/bin/mockgen &>/dev/null

    echo [FMT]
    cd $GOPATH/src/gitlab.com/lco-utils/mockgenerator
    go list -f '{{.Dir}}' ./... | grep -v /vendor/ | xargs -L1 goimports -w || return $?

    echo [BUILD]
    cd $GOPATH/bin
	go build -o "${GOPATH}/bin/mockgen" "gitlab.com/lco-utils/mockgenerator/cmd/mockgen" || return $?
	chmod 755 $GOPATH/bin/mockgen

    echo [VET]
    cd $GOPATH/src/gitlab.com/lco-utils/mockgenerator
    go list ./... | grep -v /vendor/ | xargs -L1 godep go vet || return $?

    # echo [LINT]
    # cd $GOPATH/src/gitlab.com/lco-utils/mockgenerator
    # go list ./... | grep -v '/vendor/\|/mock$' | xargs -L1 fgt golint || return $?
}

cd "${GOPATH}"

build

exit $?
